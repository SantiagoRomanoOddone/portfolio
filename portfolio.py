class Money:
    def __init__(self, amount, currency):
        self._amount = amount
        self._currency = currency

    def __eq__(self, other):
        return self._amount == other._amount and self._currency == other._currency

    def dollar(amount):
        return Money(amount,"USD")

    def franc(amount):
        return Money(amount,"CHF")
    
    def times(self, multiplier):
        return Money(self._amount * multiplier, self._currency)